# Example setup for automated credential management

## To replicate

1. Prepare ArgoCD
    ```shell
    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```
2. Generate a key pair for repository access and add the private key to k8s Secret
    ```shell
    ssh-keygen -t rsa -b 4096 -f demo-key
    kubectl create secret -n argocd generic demo-repo-ssh-key --from-file=sshPrivateKey=demo-key
    ```
3. Add `demo-key.pub` SSH public key to the repo settings in your remote

4. Apply `apply-me.yaml`
    ```shell
    kubectl apply -f apply-me.yaml
    ```


## Features

### Vault Operator

Vault Operator from BanzaiCloud allows to easily manage production Vault clusters and is fully compatible with GitOps. 

See https://banzaicloud.com/docs/bank-vaults/operator/

Installation example [here](templates/vault-operator.yaml)


### Vault Secrets Webhook

BanzaiCloud also provides a convenient injection mechanism to directly inject secrets into pod environment.

See https://banzaicloud.com/docs/bank-vaults/mutating-webhook/

Installation example [here](templates/vault-secrets-webhook.yaml)


### Secrets injection

In the [example-writer-app](apps/example-application/example-writer-app.yaml) and [mariadb-root-rotation.yaml](apps/mariadb/mariadb-root-rotation.yaml)
you can see examples of Vault secrets injection into Pods' process environment.


### Database secrets engine

In [vault.yaml](apps/vault/vault.yaml), `spec.externalConfig.secrets[example/database]` you can see reference configuration for database credentials management.

In the roles section, application developers can define specific permissions (grants) for their applications' database user accounts.

These grants can be verified during the review process before merging the PR. All such grants are committed to repo, thus tracked and version controlled.

The role also defines a TTL for the generated credentials. After the TTL the credentials are no longer valid.


### Ephemeral database credentials

When working with [Vault Database Secrets Engine](https://www.vaultproject.io/docs/secrets/databases), we can define two kinds of roles (credentials) - static and dynamic.

Dynamic role will generate unique username and unique password upon each and every request to fetch credentials (e.g. when application pod boots up and requests username and password for the database).
Such credentials will expire after a pre-configured TTL.
Upon expiration Vault will delete the user from the DB and the application will no longer be able to se the credentials.
Thus, the application must somehow request fresh credentials before the old ones expire.

In the [example-writer-app](apps/example-application/example-writer-app.yaml) you can see an example K8S deployment, which is using such ephemeral credentials.

### Automatic root credentials rotation

Vault database secrets engine allows managing root credentials of databases and other external services.

If you supply `root` user credentials of the database to Vault and let Vault rotate it just once - **no one** will be able to access the new `root` password again anymore. 

This means that **the only way** to create another highly privileged (or regular) user is to create a new role for a given database connection through Vault. Which is going to be audited and trackable.

In some cases corporate security requirements enforce "periodic rotation" of such credentials. Instead of exposing APIs and complicating our software - we can add a CronJob, which rotates such credentials periodically "just in case", for example, every day.
In the [mariadb-root-rotation.yaml](apps/mariadb/mariadb-root-rotation.yaml) example, such rotation is configured to happen every 10 minutes for demo purposes.